package solvedTasks;

public class FindSecondLargestNumber {

    private static int getSecondLargestNumber(int[] arr){

        if(arr == null){
            return 0;
        }

        int largest = arr[0];
        int secondLargest = arr[0];

        for(int i = 1; i < arr.length; i++){
            if(largest < arr[i]){
                secondLargest = largest;
                largest = arr[i];
            } else if(secondLargest < arr[i] && arr[i] != largest){
                secondLargest = arr[i];
            }
        }
        return secondLargest;
    }

    public static void main(String[] args) {
        int arr[] = {4, 6, 1, 8, 7, 23, 13, 23};
        System.out.println(getSecondLargestNumber(arr));
    }
}
