package solvedTasks;

public class RemoveWhiteSpacesTask {

    private static String removeWhiteSpaces(String str){
        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < str.length(); i++){
            char ch = str.charAt(i);
            if(ch != ' ') {
                sb.append(str.charAt(i));
            }

        }
        return sb.toString();
    }

    public static void main(String[] args) {
        System.out.println(removeWhiteSpaces("Lots o f wh i te      space           s"));
    }
}
