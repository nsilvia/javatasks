package solvedTasks;

public class ArmstrongNumber {
    private static boolean isArmstrong(int number){
//        String num = "" + number;
        String num = Integer.toString(number);
        int power = num.length();
        int sum = 0;

        for(int i = 0; i < num.length(); i++){
            int holder = Integer.parseInt(num.substring(i, i + 1));
            double poweredValue = Math.pow(holder, power);
            sum += (int)poweredValue;
        }

        return sum == number;
    }

    public static void main(String[] args) {
        System.out.println(isArmstrong(153));
    }
}
