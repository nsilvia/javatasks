package solvedTasks;

import java.util.HashMap;
import java.util.Map;

public class AnagramTask {

    private static boolean isAnagram(String str1, String str2) {
        if (str1 == null || str2 == null) {
            return false;
        }

        if (str1.length() != str2.length()) {
            return false;
        }

        Map<Character, Integer> str1Mapped = toCharsAndEncounterCount(str1);

        for (int i = 0; i < str2.length(); i++) {
            char ch = str2.charAt(i);
            if (str1Mapped.containsKey(ch)) {
                if (str1Mapped.get(ch) > 1) {
                    str1Mapped.put(ch, str1Mapped.get(ch) - 1);
                } else {
                    str1Mapped.remove(ch);
                }
            } else{
                return false;
            }

        }
        return str1Mapped.isEmpty();

    }

    private static Map<Character, Integer> toCharsAndEncounterCount(String str) {
        if (str == null) {
            //potential NPE at the called, return empty object instead
            return null;
        }

        Map<Character, Integer> charsAndEncounterCount = new HashMap<>();
        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);

            if (charsAndEncounterCount.containsKey(ch)) {
                charsAndEncounterCount.put(ch, charsAndEncounterCount.get(ch) + 1);
            } else {
                charsAndEncounterCount.put(ch, 1);
            }

        }
        return charsAndEncounterCount;
    }

    public static void main(String[] args) {
        String a = "banana manaba";
        String b = "nabasa banama";

        System.out.println(isAnagram(a, b));
    }
}
