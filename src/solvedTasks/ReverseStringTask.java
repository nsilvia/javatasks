package solvedTasks;
public class ReverseStringTask {

    private static String reverseOne(String toReverse){
        StringBuilder sb = new StringBuilder(toReverse);
        sb.reverse();
        return sb.toString();
    }

    private static String reverseTwo(String toReverse){
        StringBuilder sb = new StringBuilder();

        for(int i = toReverse.length() - 1; i >=0; i--){
            sb.append(toReverse.charAt(i));
        }
        return sb.toString();
    }

    private static String recursiveReverse(String toReverse){
        if(toReverse == null || toReverse.length() <= 1){
            return toReverse;
        }

        return recursiveReverse(toReverse.substring(1)) + toReverse.charAt(0);
    }

    public static void main(String[] args) {
        System.out.println(recursiveReverse("Java test"));
    }
}
