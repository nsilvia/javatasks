package solvedTasks;

import java.util.Arrays;

public class IntArrayEqualityTask {

    private static boolean checkEqualityOne(int[] arr1, int[] arr2) {
        //constant time

        if (arr1 == arr2) {
            return true;
        }

        if (arr1 == null || arr2 == null) {
            return false;
        }

        if (arr1.length != arr2.length) {
            return false;
        }


        for (int i = 0; i < arr1.length; i++) {
            if (arr1[i] != arr2[i]) {
                return false;
            }
        }

        return true;
    }

    private static boolean checkEqualityTwo(int[] arr1, int[] arr2) {
        return Arrays.equals(arr1, arr2);
    }

    public static void main(String[] args) {
        int[] arr1 = {3, 4, 3, 4, 5, 6, 66, 3, 56};
        int[] arr2 = {3, 4, 3, 4, 5, 6, 66, 3, 56};

        System.out.println(checkEqualityOne(arr1, arr2));
    }
}
