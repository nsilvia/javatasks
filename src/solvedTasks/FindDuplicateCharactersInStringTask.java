package solvedTasks;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class FindDuplicateCharactersInStringTask {
    public static void main(String[] args) {
        String str = "Butter is full of bees!?!";

        Set<Character> keeper = new HashSet<>();

//        Map<Character, Integer> singleCharsWithCount = new HashMap<>();
        Map<Character, Integer> duplicateCharsWithCount = new HashMap<>();

        for (int i = 0; i < str.length(); i++) {

            char ch = str.charAt(i);

//            if(singleCharsWithCount.containsKey(ch)){
//                singleCharsWithCount.put(ch, singleCharsWithCount.get(ch) + 1);
//            }else{
//                singleCharsWithCount.put(ch, 1);
//            }

            if (duplicateCharsWithCount.containsKey(ch)) {
                duplicateCharsWithCount.put(ch, duplicateCharsWithCount.get(ch) + 1);
            } else if(!keeper.add(ch)){
                duplicateCharsWithCount.put(ch, 2);
            }

        }
//        System.out.println(singleCharsWithCount);
        System.out.println(duplicateCharsWithCount);

    }

}

